const http = require("http");

//Creates a variable "port" to store the port number
const port = 4001;

const app = http.createServer((req, res) => {

	//www.facebook.com/profile
	//localhost:4001/greeting
	//"/greeting" endpoint
	if(req.url == "/greeting"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Hello Again!");
	}else if(req.url == "/homepage"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("This is the homepage!");
	}else {
		res.writeHead(404, {"Content-Type" : "text/plain"});
		res.end("404: Page not found");
	}

})

app.listen(port);

console.log(`Server now accesssible at localhost:${port}`);
console.log(`Server now accesssible at localhost:${port}`);


//Installing nodemon so it will automatically restart no need to cancel & redo on gitbash
// npm i -g nodemon