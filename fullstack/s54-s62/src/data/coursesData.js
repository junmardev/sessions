const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at dapibus urna. Sed egestas orci non commodo pellentesque. In blandit nec magna non rutrum.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Vivamus quis orci in eros dignissim dignissim in quis purus. Quisque tincidunt at lacus et porta. Nam convallis blandit urna, ullamcorper laoreet neque tincidunt ut. Aliquam non lobortis odio. Integer vel porta eros.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Proin suscipit fermentum varius. Sed porta elit ex, et efficitur ipsum euismod sit amet. Ut vel orci ornare ex posuere laoreet. Cras turpis sem, sodales non purus in, hendrerit placerat elit.",
		price: 55000,
		onOffer: true
	}
]

export default coursesData;