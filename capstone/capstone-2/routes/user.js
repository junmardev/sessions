const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth.js")

// Destructure from auth
const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const router = express.Router();



//User Registration
router.post("/register", userController.registerUser);

//Check if user/email already Exist
router.post("/checkEmail", userController.checkEmailExist);

//User authentication
router.post("/login", userController.loginUser);

//User Reset Password
router.put("/reset-password", verify, userController.resetPassword);

//NEW ADDED
//[SECTION] Update Profile  
router.get("/details", verify, userController.getProfile);

//get all products
router.get("/all", userController.getAllProducts);

//get all active products
router.get("/", userController.getAllActive);

//[SECTION] Update Profile  
router.put('/profile', verify, userController.updateProfile);

//[SECTION] Route for Search Course by Name
    router.post('/searchByName', userController.searchProductsByName); 

//[ACTIVITY] Search Courses By Price Range
    router.post('/searchByPrice', userController.searchProductsByPriceRange);


// [SECTION] Export Route system
module.exports = router;