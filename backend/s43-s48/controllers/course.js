const Course = require("../models/Course.js");
// const bcrypt = require("bcrypt");
// const auth = require("../auth");

// ADDING A COURSE ACTIVITY

module.exports.addCourse = (req, res) => {
	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});

	return newCourse.save().then((course, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
	.catch(err => res.send(err));
}


module.exports.getAllCourses = (req, res) => {
	return Course.find({}).then(result => {
		if(result.length === 0){
			return res.send("There is no courses in the DB.");
		}else {
			return res.send(result);
		}
	})
}

module.exports.getAllActive = (req, res) => {
	return Course.find({isActive : true}).then(result => {
		if(result.length === 0){
			return res.send("There is currently no active courses.");
		}else {
			return res.send(result);
		}
	})
}

module.exports.getCourse = (req, res) => {
	return Course.findById(req.params.courseId).then(result => {
		if(result === 0){
			return res.send("Cannot find course with the provided ID.");
		} else {
			if(result.isActive === false){
				return res.send("The course you're trying to access is not available.");
			} else {
				return res.send(result);
			}
		}
	})
	.catch(error => res.send("Please enter a correct course ID"));
}

module.exports.updateCourse = (req, res) => {
	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
}

// ACTIVITY - Archive and Activate
//Archive a Course
module.exports.archiveCourse = (req, res) => {
	let updatedActiveCourse = {
		isActive: false
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedActiveCourse).then((course, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
	.catch(error => res.send("Please enter a correct course ID"));
}

//Activate an Archive Course
module.exports.activateCourse = (req, res) => {
	let updatedArchiveCourse = {
		isActive: true
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedArchiveCourse).then((course, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
	.catch(error => res.send("Please enter a correct course ID"));
}

