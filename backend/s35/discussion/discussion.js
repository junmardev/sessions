// [SECTION] Comparison Query Operator

// $gt / $gte operator -> greater than / greater than equal

/*

SYNTAX: 

db.collectionName.find({field: {$gt: value}});
db.collectionName.find({field: {$gte: value}});

*/

db.Users.find({age: {$gt : 50}});
db.Users.find({age: {$gte : 50}});


// $lt / $lte operator -> less than/ less than equal

/*

SYNTAX: 

db.collectionName.find({field: {$lt: value}});
db.collectionName.find({field: {$lte: value}});

*/

db.Users.find({age: {$lt : 50}});
db.Users.find({age: {$lte : 50}});

// $ne operator -> not equal 

/*

SYNTAX: 

db.collectionName.find({field: {$ne: value}});


*/

db.Users.find({age: {$ne : 76}});

// $in operator -> in only no other term or nasa in tagalog

/*

SYNTAX: 

db.collectionName.find({field: {$in: value}});


*/

db.Users.find({lastName : {$in : ["Hawking", "Doe"]}});
db.Users.find({courses : {$in : ["HTML", "React"]}});

// [SECTION] Logical Query Operator

/*

SNYTAX: 

db.Users.find({$or: [{fieldA: value}, {fieldB: value}]});


*/

db.Users.find({$or: [{firstName: "Neil"}, {age: 25}]});

//$or with gt
db.Users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});


// $and operator
/*

SNYTAX: 

db.Users.find({$and: [{fieldA: value}, {fieldB: value}]});


*/

db.Users.find({$and: [{age: {$ne: 76}}, {age: {$ne: 27}}]});



// [SECTION] Field Projection

// Inclusion
/*
- Allows us to include/add specific fields only when retrieving documents.
- The value provided is 1 to denote that the field is being included.
- Syntax
    db.Users.find({criteria},{field: 1}) -> 1 is true 0 is false
*/

db.Users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1
}
);


db.Users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	"contact.email": 1
}
);


// Exclusion

db.Users.find(
{
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
}
);


// Suppressing the ID Field

db.Users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id: 0
}
);

//[SECTION] Evaluation Query Operator

//$regex operator

/*

SYNTAX: 

db.Users.find({field: {$regex: "pattern", $option : "$optionValue"}});

*/

//Case Sensitive Query

db.Users.find({firstName: {$regex: "N"}});

//Case Insensitive Query

//MongoDB v4 /i -> $i -> i

db.Users.find({firstName: {$regex: "j", $options: "i"}});

db.Users.find({firstName: {$regex: "n", $options: "i"}});


