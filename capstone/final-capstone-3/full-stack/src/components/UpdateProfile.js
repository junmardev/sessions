import React, { useState } from 'react';

const UpdateProfileForm = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    const token = localStorage.getItem('token');

    const profileData = {
      name,
      email,
      mobileNo,
    };

    fetch('https://capstone2-letigio.onrender.com/users/profile', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(profileData),
    })
    .then(res=>res.json())
    .then((response) => {
        if (response._id) {
          // Profile update was successful, refresh the page
          window.location.reload();
        } else {
          throw new Error('Profile update failed');
        }
      })
      .catch((error) => {
        console.error(error);
        // Handle error here
      });
  };

  return (
    <div className='container' style={{ maxWidth: '50%', margin: '0 auto' }}>
        <h2 className='my-4'>Update Profile</h2>
        <form onSubmit={handleSubmit}>
        <div className="mb-3">
            <label htmlFor="name" className="form-label">
            Name
            </label>
            <input
            type="text"
            className="form-control"
            id="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            />
        </div>
        <div className="mb-3">
            <label htmlFor="email" className="form-label">
            Email
            </label>
            <input
            type="email"
            className="form-control"
            id="lastName"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            />
        </div>
        <div className="mb-3">
            <label htmlFor="mobileNo" className="form-label">
            Mobile Number
            </label>
            <input
            type="text"
            className="form-control"
            id="mobileNo"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
            />
        </div>
        <button type="submit" className="btn btn-success">
            Update Profile
        </button>
        </form>
    </div>
  );
};

export default UpdateProfileForm;