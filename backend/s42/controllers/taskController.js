const Task = require("../models/Task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		//req.body.name === requestBody.name
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
		}else{
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removeTask, error) => {
		if(error) {
			console.log(error)
		} else {
			return removeTask;
		}
	})
}

module.exports.updateTask = (taskId, requestBody) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return res.send("cannot find data with the provided id");
		}

		result.name = requestBody.name;

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error);
				return res.send("There is an error while saving the update.")
			}else{
				return updatedTask;
			}
		})
	})
}

//Activity Get Specific


module.exports.getSpecificTask = (specificGetParams) => {
	return Task.findById(specificGetParams).then((result, error) => {
		if(error){
			console.log(error);
			return res.send("cannot find data with the provided ID");
		} else {
			return result;
		}
	})
}

module.exports.updateTaskStatus = (requestParams, requestBody) => {
	return Task.findById(requestParams.id).then((result, error) => {
		if(error){
			console.error(error);
		}else{
			result.status = requestParams.stt;
			return result.save().then((savedTask, saveErr) => {
				if(saveErr){
					console.error(saveErr);
				}else{
					return savedTask;
				}
			});
		}
	})
}