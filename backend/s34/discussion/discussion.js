//CRUD Operations (MongoDB)

//CRUD Operations are the heart of any backend app.
//Mastering CRUD Operations is Essential for any developers.

/*

C - Create/Insert
R - Retrieve/Read
U - Update
D - Delete

*/

// [SECTION] Creating Documents

/*

SYNTAX: 

db.Users.insertOne({object})

*/

db.Users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JS", "PYTHON"],
	department: "none"
});

db.Users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 21,
	contact: {
		phone: "00000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

//This is my own data created

db.Users.insertOne({
	firstName: "Junmar",
	lastName: "Letigio",
	age: 27,
	contact: {
		phone: "091112223363664",
		email: "junmarsample@gmail.com"
	},
	course: ["Front End", "Back End", "Full Stack"],
	department: "none"
});



/*

SYNTAX: 

db.Users.insertMany({object})

*/

db.Users.insertMany([
{
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JS", "PYTHON"],
	department: "none"
},

{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	},
	courses: ["PHP", "React", "PYTHON"],
	department: "none"
},

{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}
]);




// [SECTION] - Read/Retrieve

/*

SYNTAX: 

db.Users.findOne();
db.Users.findOne({field: value});

*/

db.Users.findOne();

db.Users.findOne({firstName: "Stephen"});


/*

SYNTAX: 

db.Users.findMany();
db.Users.findMany({field: value});

*/

db.Users.find({department: "none"});

//Multiple Criteria

db.Users.find({department: "none", age: 82});




// [SECTION] Updating a data

/*

SYNTAX: 

db.collectionName.updateOne({criteria}, {$set: {field: value}})

*/

db.Users.updateOne(
{firstName: "Test"},
{
	$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "123456789",
			email: "billgates@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	}
}
);

// find the data

db.Users.findOne({firstName: "Bill"});
db.Users.findOne({firstName: "Test"});

db.Users.findOne({"contact.email": "billgates@gmail.com"});


//Update many collection

/*

db.Users.updateMany({criteria}, {$set: {field: value}});

*/

db.Users.updateMany(
{department: "none"},
{
	$set: {
		department: "HR"
	}
}
);



//[SECTION] Deleting a data
//Inserted/Added new to be deleted

db.Users.insert({
	firstName: "Test"
});

//Delete single document

/*

SYNTAX: 

db.Users.deleteOne({criteria});

*/

db.Users.deleteOne({
	firstName: "Test"
});

//Delete Many
/*

db.Users.deleteMany({criteria});
OR
db.Users.deleteMany({firstName: "Bill"});

*/
//Insert another bill

db.Users.insert({
	firstName: "Bill"
});

db.Users.deleteMany({firstName: "Bill"});