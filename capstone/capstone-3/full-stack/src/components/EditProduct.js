 import { Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';


export default function EditProduct({productData, fetchData}){
	// console.log(productData);
	//state for course id which will be use for fetching
	const [productId, setProductId] = useState("");
	// console.log(productId);
	//state for editcourse modal
	const [showEdit, setShowEdit] = useState(false);

	//usestate for our form (modal)
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	//function for opening edit modal
	const openEdit = (productId) => {
		setShowEdit(true);

		//to still get the actual data from the form
		fetch(`https://capstone2-letigio.onrender.com/sellerAdmin/view/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			// console.log("Hello World!");
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName("");
		setDescription("");
		setPrice(0);
	}
 
	//function to save our update
	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`https://capstone2-letigio.onrender.com/sellerAdmin/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Update Success!",
					icon: "success",
					text: "Product Successfully Updated!"
				})
				fetchData();
				closeEdit();
			}else {
				Swal.fire({
					title: "Update Error!",
					icon: "error",
					text: "Please try again!"
				})
				fetchData();
				closeEdit();
			}
		})
	}

 
	return (

		<>
			<Button variant="primary" size="sm" onClick={() => {openEdit(productData)}}>Edit</Button>

			 <Modal show={showEdit} onHide={closeEdit}>
                    <Form onSubmit={e => editProduct(e, productId)}>
                        <Modal.Header closeButton>
                                <Modal.Title>Edit Product</Modal.Title>
                        </Modal.Header>
                         <Modal.Body>                             
                                <Form.Group controlId="productName">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control type="text" required value={name} onChange={e => setName(e.target.value)}/>
                                </Form.Group>
                                <Form.Group controlId="productDescription">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control type="text" required value={description} onChange={e => setDescription(e.target.value)}/>
                                </Form.Group>
                                <Form.Group controlId="productPrice">
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control type="number" required value={price} onChange={e => setPrice(e.target.value)}/>
                                </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                                <Button variant="secondary" onClick={closeEdit}>Close</Button>
                                <Button variant="success" type="submit">Submit</Button>
                        </Modal.Footer>
                    </Form>
            </Modal>
		</>

	)
}