import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';

export default function Home() {

    const data = {
    	/* textual contents*/
        title: "J.L Gadgets",
        content: "We offer laptops, phones, and top-notch after-sales support.",
        /* buttons */
        destination: "/products",
        label: "Order Now!"
    }
  
    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}