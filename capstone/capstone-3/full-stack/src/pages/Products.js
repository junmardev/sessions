import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../pages/AdminView';
 
export default function Product() {

    const { user } = useContext(UserContext);

    const [products, setProducts] = useState([]);

    const fetchData = () => {
        fetch('https://capstone2-letigio.onrender.com/users/all')
        .then(res => res.json())
        .then(data => {
            
            console.log(data);

            setProducts(data);

        });
    }

    useEffect(() => {

        fetchData()

    }, []);

    return (
        <>
            {
                (user.isAdmin === true) ?
                    <AdminView productsData={products} fetchData={fetchData} />
                    :
                    <UserView productsData={products} />

            }
        </>
    )
}