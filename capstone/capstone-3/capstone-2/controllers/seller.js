//[SECTION] Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Create/Add a Product
module.exports.addProduct = (req, res) => {
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.isActive,
		createdOn: req.createdOn,
		image: req.body.image
	})

	return newProduct.save().then((product, error) => {
		if(error) {
			return res.send(false)
		}else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
}


// Retrieve all active Products
module.exports.allProducts = (req, res) => {
	return Product.find({}).then(result => {
		if(result.length === 0){
			return res.send(false);
		}else {
			return res.send(result);
		}
	})
}


//Retrieve Active Products
module.exports.getAllActiveProducts = (req, res) => {
	return Product.find({isActive : true}).then(result => {
		if(result.length === 0){
			return res.send(false);
		}else {
			return res.send(result);
		}
	})
}


//Retrieve Single Product
module.exports.getSingleProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result => {
		if(result === 0){
			return res.send(false);
		} else {
			if(result.isActive === false){
				return res.send(false);
			} else {
				return res.send(result);
			}
		}
	})
	.catch(error => res.send("Please enter a correct product ID"));
}


//Updating Product Information - Seller / Admin Only
module.exports.updateProduct = (req, res) => {
	let updateAProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Product.findByIdAndUpdate(req.params.productId, updateAProduct).then((product, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
}


//Archive Product - Admin Only
module.exports.archiveProduct = (req, res) => {
	let archiveActiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(req.params.productId, archiveActiveProduct).then((product, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
	.catch(error => res.send("Please enter a correct product ID"));
}


//Activate Product - Admin Only
module.exports.activateProduct = (req, res) => {
	let activateArchiveProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(req.params.productId, activateArchiveProduct).then((product, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
	.catch(error => res.send("Please enter a correct product ID"));
}

//Set User as Admin/Seller
module.exports.setUserAsAdmin = (req, res) => {
	let setAUserAsAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(req.params.userId, setAUserAsAdmin).then((setAdmin, error) => {
		if(error) {
			return res.send(false);
		}else {
			return res.send(true);
		}
	})

	.catch(error => res.send("Please enter a correct user ID"));

}
