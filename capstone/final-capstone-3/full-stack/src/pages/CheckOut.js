import React, { useState } from 'react';
import { Container, Card, Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import ProductView from './ProductView';

const CheckOut = () => {
  const navigate = useNavigate();
  const { productId, quantity } = useParams();

  const [paymentMethod, setPaymentMethod] = useState('');
  const [debitCreditNumber, setDebitCreditNumber] = useState('');
  const [cashOnDeliveryAddress, setCashOnDeliveryAddress] = useState('');

  const handleSubmitOrder = () => {
    if (paymentMethod === 'debitCredit' && validateDebitCreditNumber(debitCreditNumber)) {
      Swal.fire({
        title: 'Successfully Order!',
        icon: 'success',
        text: 'Your order has been placed successfully.',
      });
      navigate('/products');
    } else if (paymentMethod === 'cashOnDelivery' && cashOnDeliveryAddress.trim() !== '') {
      Swal.fire({
        title: 'Successfully Order!',
        icon: 'success',
        text: 'Your order has been placed successfully.',
      });
      navigate('/products');
    } else {
      Swal.fire({
        title: 'Invalid Payment Details',
        icon: 'error',
        text: 'Please provide valid payment information.',
      });
    }
  };

  const validateDebitCreditNumber = (number) => {
    return /^[0-9]{15,16}$/.test(number);
  };

  // Use the dropdown (select) for payment method selection
  const handlePaymentMethodChange = (event) => {
    setPaymentMethod(event.target.value);
  };

  // Use the ternary operator to determine the button status (active or disabled)
  const isSubmitButtonActive = (paymentMethod === 'debitCredit' && validateDebitCreditNumber(debitCreditNumber)) || 
                            (paymentMethod === 'cashOnDelivery' && cashOnDeliveryAddress.trim() !== '');

  return (
    <Container className="mt-5" style={{ maxWidth: '50%' }}>
      <Card>
        <Card.Body>
          <Card.Title className="text-center">Checkout Page</Card.Title>
          <Form>
            <Form.Group controlId="paymentMethod">
              <Form.Label className="mt-5">Select Payment Method: </Form.Label>
              <Form.Control as="select" value={paymentMethod} onChange={handlePaymentMethodChange}>
                <option value="">Select Payment Method</option>
                <option value="debitCredit">Debit/Credit Card</option>
                <option value="cashOnDelivery">Cash On Delivery</option>
              </Form.Control>
            </Form.Group>
            {paymentMethod === 'debitCredit' && (
              <Form.Group controlId="debitCreditNumber">
                <Form.Label className="mt-3 text-center">Debit/Credit Card Number: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter card number"
                  value={debitCreditNumber}
                  onChange={(e) => setDebitCreditNumber(e.target.value)}
                />
              </Form.Group>
            )}
            {paymentMethod === 'cashOnDelivery' && (
              <Form.Group controlId="cashOnDeliveryAddress">
                <Form.Label className="mt-3">Delivery Address: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter delivery address"
                  value={cashOnDeliveryAddress}
                  onChange={(e) => setCashOnDeliveryAddress(e.target.value)}
                />
              </Form.Group>
            )}
            <div className="d-flex justify-content-center">
              <Button variant="success" className="mt-4" onClick={handleSubmitOrder} disabled={!isSubmitButtonActive}>
                Submit Order
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </Container>
  );
};

export default CheckOut;

