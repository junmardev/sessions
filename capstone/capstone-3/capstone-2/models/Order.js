const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: [true, "User ID is required for the order."]
  },
  products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
        required: [true, "Product ID is required for each item in the order."]
      },
      quantity: {
        type: Number,
        required: [true, "Quantity is required for each item in the order."]
      }
    }
  ],
  totalAmount: {
    type: Number,
    required: [true, "Total amount is required for the order."]
  },
  purchasedOn: {
    type: Date,
    default: new Date()
  }
})

module.exports = mongoose.model("Order", orderSchema);
