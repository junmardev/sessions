// [SECTION] Dependencies and Modules
const express = require("express");
const courseController = require("../controllers/course.js");
const auth = require("../auth.js");

// destructuring of verify and verifyAdmin

const {verify, verifyAdmin} = auth;

const router = express.Router();


// ADDING A COURSE ACTIVITY
// Create a course POST method

router.post("/", verify, verifyAdmin, courseController.addCourse);

// Get All course
router.get("/all", courseController.getAllCourses);

// Get All "active" course

router.get("/", courseController.getAllActive);

// Get one specific course using it's ID

router.get("/view/:courseId", courseController.getCourse);

// Updating a Course (Admin Only)

router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// ACTIVITY - Archive and Activate
//Archive a Course
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);
//Activate an Archive Course
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);


module.exports = router;