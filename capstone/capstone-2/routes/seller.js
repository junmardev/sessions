const express = require("express");
const sellerController = require("../controllers/seller");
const auth = require("../auth.js")
 
// Destructure from auth
const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const router = express.Router();

// Create/Add a Product
router.post("/addProduct", verify, verifyAdmin, sellerController.addProduct);

// Retrieve all Products
router.get("/allProducts", sellerController.allProducts);

//Retrieve all Active Products
router.get("/activeProducts", sellerController.getAllActiveProducts);

//Retrieve Single Product
router.get("/view/:productId", sellerController.getSingleProduct);

//Updating Product Information - Seller / Admin Only
router.put("/:productId", sellerController.updateProduct);

//Archive Product - Admin Only
router.put("/:productId/archive", verify, verifyAdmin, sellerController.archiveProduct);

//Activate Product - Admin Only
router.put("/:productId/activate", verify, verifyAdmin, sellerController.activateProduct);

//Set User as Admin/Seller
router.put("/:userId/setAsAdmin", verify, verifyAdmin, sellerController.setUserAsAdmin);

module.exports = router;