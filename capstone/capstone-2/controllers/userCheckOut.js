const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

 
//User checkout -> Non-admin/seller checkout (Creating/Placing an Order)
module.exports.userCheckOut = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send(false);
  }

  try {
    const user = await User.findById(req.user.id);
    if (!user) {
      return res.status(404).send(false);
    }

    const product = await Product.findById(req.body.productId);
    if (!product) {
      return res.status(404).send(false);
    }

    // Check if the product is active true
    if (!product.isActive) {
      return res.status(400).send(false);
    }

    // Calculate the total amount based on product price and quantity
    const totalPrice = product.price * req.body.quantity;

    const order = new Order({
      userId: req.user.id,
      userName: user.name, // Add user's name to the order
      products: [
        {
          productId: req.body.productId,
          productName: product.name, // Add product name to the order
          quantity: req.body.quantity,
        },
      ],
      totalAmount: totalPrice,
    });

    await order.save();

    return res.send(true);
  } catch (error) {
    return res.status(500).send("An error occurred while processing your request");
  }
};



//User Order Summary
module.exports.orderSummary = (req, res) => {
  Order.find({ userId: req.user.id })
    .populate('userId', 'name') // Populate user's name
    .populate('products.productId', 'name') // Populate product name
    .then(orders => {
      if (orders.length === 0) {
        return res.send("You have no orders!");
      } else {
        // I can format this order summary as needed -> will get back into this.
        return res.send(orders);
      }
    })
    .catch(error => res.send(error));
};


//Retrieve All Orders Seller/Admin Only
module.exports.getAllOrders = (req, res) => {
  return Order.find({})
  .populate('userId', 'name') // Populate user's name
  .populate('products.productId', 'name') // Populate product name
  .then(result => {
    if(result.length === 0){
      return res.send(false);
    }else {
      return res.send(result);
    }
  })
}

//NEW ADDED
module.exports.getProduct = (req, res) => {
  return Product.findById(req.params.productId).then(result =>{
/*    if(result === 0){
      return res.send("Cannot find course with the provided ID.")
    }else{
      return res.send(result);
    }*/

    if(result === 0){
      return res.send("Cannot find product with the provided ID.")
    }else{
      if(result.isActive === false){
        return res.send("The product you are trying to access is not available.");
      }else{
        return res.send(result);
      }
    }
  })
  .catch(error => res.send("You provided a wrong product ID"));
}
