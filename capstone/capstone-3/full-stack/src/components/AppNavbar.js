import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar'; 
import '../App.css'
import UserContext from '../UserContext';
import { useState, useContext } from 'react';

 
export default function AppNavbar() {

	const navbarStyle = {
	    background: 'dark', 
	    borderBottom: '3px solid skyblue', 
  	};

  	const { user } = useContext(UserContext);

	return (

		    <Navbar style={navbarStyle} variant="dark" expand="lg">
	            <Container fluid>
	                <Navbar.Brand as={Link} to="/">J.L Gadgets</Navbar.Brand>
	                <Navbar.Toggle aria-controls="basic-navbar-nav" />
	                <Navbar.Collapse id="basic-navbar-nav">
	                    <Nav className="ms-auto">
	                        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
	                       	<Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
	                       	{(user.id !== null) ? 

	                       	        user.isAdmin 
	                       	        ?
	                       	        <>
	                       	            <Nav.Link as={Link} to="/addProduct">Add Product</Nav.Link>
	                       	            <Nav.Link as={Link} to="/all-users/order-history">Order History</Nav.Link>
	                       	            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
	                       	        </>
	                       	        :
	                       	        <>
	                       	            <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
	                       	            <Nav.Link as={Link} to="/order-history">Order History</Nav.Link>
	                       	            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
	                       	        </>
	                       	    : 
	                       	        <>
	                       	            <Nav.Link as={Link} to="/login">Login</Nav.Link>
	                       	            <Nav.Link as={Link} to="/register">Register</Nav.Link>
	                       	        </>
	                       	}
	                    </Nav>
	                </Navbar.Collapse>
	            </Container>
            </Navbar>
	)
}