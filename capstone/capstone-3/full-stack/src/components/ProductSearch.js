import React, { useState } from 'react';
import ProductCard from './ProductCard';
import { Button } from 'react-bootstrap';

const ProductSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch('https://capstone2-letigio.onrender.com/users/searchByName', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName : searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for products:', error);
    }
  };

  const clearSearchResults = () => {
    setSearchResults([]);
    setSearchQuery('');
  };

  return (
    <div className='pt-5 container bg-p'>
      <h2>Product Search</h2>
      <div className="form-group">
        <label htmlFor="productName">Product Name:</label>
        <input
          type="text"
          id="productName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      <h3>Search Results:</h3>
      <ul>
        {searchResults.length > 0 && (
          <Button className="btn btn-danger pl-0 ml-0 mb-1" onClick={clearSearchResults}>
          Close Search
          </Button>
        )}
        {searchResults.map(product => (
        //   <li key={course._id}>{course.name}</li>
        <div className="p-3 bg-success">
            <ProductCard productProp={product} key={product._id}/>
        </div>
        ))}
      </ul>
    </div>
  );
};

export default ProductSearch;