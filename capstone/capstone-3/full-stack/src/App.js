import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import AppNavbar from './components/AppNavbar';
import './App.css';
import Home from './pages/Home';
import Login from './pages/Login';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import CheckOut from './pages/CheckOut';
import Orders from './pages/UserOrder';
import AllOrders from './pages/AdminOrder';
import Logout from './pages/Logout';
import AddProduct from './pages/AddProduct';
import Profile from './pages/Profile';
import Register from './pages/Register';
import Error from './pages/Error';
  
function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

    // Function for clearing localStorage on logout
  const unsetUser = () => {

    localStorage.clear();

  };

   useEffect(() => {

    // console.log(user);
    fetch(`https://capstone2-letigio.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });

      // Else set the user states to the initial values
      } else {

        setUser({
          id: null,
          isAdmin: null
        });

      }

    })

    }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
      <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/checkOut/:productId" element={<ProductView />}/>
            <Route path="/checkout/:productId/:quantity" element={<CheckOut />}/>
            <Route path="/order-history" element={<Orders />}/> 
            <Route path="/all-users/order-history" element={<AllOrders />}/> 
            <Route path="/login" element={<Login />} />
            <Route path="/addProduct" element={<AddProduct />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/register" element={<Register />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
