// [SECTION] Dependencies and Modules
const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Token Creation

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
};


module.exports.verify = (req, res, next) => {
	//Token is retrieve from the request header
	//Authorization (Auth Tab) > Bearer Token

	console.log(req.headers.authorization);

	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth: "Failed, No Token!"})
	} else {
		console.log(token);

		/*

		Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0ZmIxYzliMTk2OWQ2YTVmZDdhMDE2NiIsImVtYWlsIjoiam9obkBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjk0NDMwMzA4fQ.Oj-Y677C6wOhUR0hdWhifvBceZBqJAlfJuROnnkFYac
		*/

		token = token.slice(7, token.length);
		console.log(token);

		jwt.verify(token, secret, function(error, decodedToken) {
			if(error){
				return res.send({
					auth: "Failed",
					message: error.message
				});
			} else {
				console.log(decodedToken);
				// Contains data from our token
				req.user = decodedToken;

				next();
				// Will let us proceed to the next controller
			}
		})
	}
}

// Verify if user account is admin or not

module.exports.verifyAdmin = (req, res, next) => {
	if(req.user.isAdmin){
		next()
	} else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
	
}





