 import { Card, Button } from 'react-bootstrap';
import coursesData from '../data/coursesData';
import {useState} from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
	//Checks to see if the data was successfully passed
	// console.log(props);
	//Every components receives information in a form of an object
	// console.log(typeof props);
	const { _id, name, description, price } = courseProp;

	//Use the state hook in this component to be able to store its state
	/*

	Syntax: 
		const [getter, setter] = useState(initialGetterValue);
	*/
	// Create a 'seats' state with an initial value of 30
 //  	const [seats, setSeats] = useState(30);

	// const [count, setCount] = useState(0);

	// console.log(useState(0));

	//Function that keeps track of the enrollees for a course
	// function enroll(){
	// 	if(seats > 0){
	// 		setCount(count + 1);
	// 		setSeats(seats - 1);
	// 	}else {
	// 		alert("No more seats");
	// 	}
	// }

	return (
		<Card>
		    <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        <Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>{description}</Card.Text>
		        <Card.Subtitle>Price:</Card.Subtitle>
		        <Card.Text>PhP {price}</Card.Text>
		        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		    </Card.Body>
		</Card>

	)
}

// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}