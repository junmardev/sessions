 //[SECTION] Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User Registration
module.exports.registerUser = (req, res) => {
	let newUser = new User ({
		name: req.body.name,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		isAdmin: req.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error){
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(err => err);
}

//Check if user/email already Exist
module.exports.checkEmailExist = (req, res) => {
	return User.find({ email: req.body.email }).then(result => {

		if(result.length > 0){
			return res.send(true)
		} else {
			return res.send(false)
		}
	})
}


//User authentication
module.exports.loginUser = (req, res) => {
	return User.findOne({ email: req.body.email }).then(result => {
		console.log(result);
		if(result === null){
			return res.send(false); // the email doesn't exist in our DB
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				return res.send({ access: auth.createAccessToken(result) })
			} else {
				return res.send(false); // Password do not match
			}
		}
	})
	.catch(err => res.send(err));
}

 
//User Reset Password
module.exports.resetPassword = async (req, res) => {
	try{
		const newPassword = req.body.newPassword;
		const userId = req.user.id;

		// Hashing the new password
		const hashedPassword = await bcrypt.hash(newPassword, 10);

		await User.findByIdAndUpdate(userId, {password: hashedPassword});

		res.status(200).json({message: true});
	}catch(error) {
		res.status(500).json({message: false});
	}
}

//NEW ADDED 
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "";
		return res.send(result);
	})
	.catch(error => error)
}

module.exports.getAllProducts = (req, res) => {
	return Product.find({}).then(result => {
		if(result.length === 0){
			return res.send("There is no product in the DB.");
		}else{
			return res.send(result);
		}
	})
}


module.exports.getAllActive = (req, res) => {
	return Product.find({isActive : true}).then(result => {
		if(result.length === 0){
			return res.send("There is currently no active courses.")
		}else{
			return res.send(result);
		}
	})
}

module.exports.updateProfile = async (req, res) => {
    try {

        console.log(req.user);
        console.log(req.body);
        
    // Get the user ID from the authenticated token
      const userId = req.user.id;
  
      // Retrieve the updated profile information from the request body
      const { name, email, mobileNo } = req.body;
  
      // Update the user's profile in the database
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        { name, email, mobileNo },
        { new: true }
      );
  
      res.send(updatedUser);
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Failed to update profile' });
    }
  }

module.exports.searchProductsByName = async (req, res) => {
    try {
      const { productName } = req.body;
  
      // Use a regular expression to perform a case-insensitive search
      const products = await Product.find({
        name: { $regex: productName, $options: 'i' }
      });
  
      res.json(products);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
};

module.exports.searchProductsByPriceRange = async (req, res) => {
      try {
        const { minPrice, maxPrice } = req.body;
    
        // Find courses within the price range
        const products = await Product.find({
          price: { $gte: minPrice, $lte: maxPrice }
        });
    
        res.status(200).json({ products });
      } catch (error) {
        res.status(500).json({ error: 'An error occurred while searching for products' });
      }
  };

