import { Card, Button, Col, Row, Container } from 'react-bootstrap';
import {useState} from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}){

	const { _id, name, description, price, image } = productProp;

 
	return (
		// <Card className="mt-4">
		//     <Card.Body>
		//     		<img src={image} className="img-fluid" style={{ width: "18rem", height: "25rem" }} />
		//         <Card.Title>{name}</Card.Title>
		//         <Card.Subtitle>Description:</Card.Subtitle>
		//         <Card.Text>{description}</Card.Text>
		//         <Card.Subtitle>Price:</Card.Subtitle>
		//         <Card.Text>PhP {price}</Card.Text>
		//         <Link className="btn btn-primary" to={`/checkOut/${_id}`}>View Product</Link>
		//     </Card.Body>
		// </Card>
    <Col lg={3} md={4} sm={6} xs={12} className="d-inline-block">
      <Card className="mt-4 h-100" style={{ maxWidth: '20rem', margin: '10px' }}>
          <div style={{ height: '30%', overflow: 'hidden' }}>
            <img src={image} className="img-fluid" alt={name} style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
          </div>
          <Card.Body style={{ maxHeight: '50%', overflow: 'auto' }}>
              <Card.Title style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text style={{ maxHeight: '50px', overflow: 'auto' }}>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Link className="btn btn-primary" to={`/checkOut/${_id}`}>
                View Product
              </Link>
          </Card.Body>
      </Card>
    </Col>
  )
}


ProductCard.propTypes = {
  productProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}