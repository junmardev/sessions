import React, { useState, useEffect } from 'react';
import { Container, Card, ListGroup } from 'react-bootstrap';


function UserOrder() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {

    fetch('https://capstone2-letigio.onrender.com/checkOut/order-summary', {
      method: 'GET',
      headers: {
        "Content-Type" : "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
    })
      .then(response => response.json())
      .then(data => {
        setOrders(data);
      })
      .catch(error => {
        console.error('Error fetching order history:', error);
      });
  }, []);

  return (
    <Container className="mt-5 text-center">
      <h2>Order History</h2>
      {orders.length === 0 ? (
        <p>You have no orders!</p>
        ) : (
        <ListGroup className="mx-auto" style={{ maxWidth: '600px' }}>
          {orders.map(order => (
            <Card key={order._id} className="mb-3">
              <Card.Body>
                <Card.Title>Purchase Date: {order.purchasedOn}</Card.Title>
                <Card.Text>Total Amount: {order.totalAmount}</Card.Text>
                <ListGroup variant="flush">
                  {order.products.map(product => (
                    <ListGroup.Item key={product._id}>
                      <p>Product: {product.productId.name}</p>
                      <p>Quantity: {product.quantity}</p>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              </Card.Body>
            </Card>
          ))}
        </ListGroup>
      )}
    </Container>
  );
}

export default UserOrder;