import { Row, Col, Card } from 'react-bootstrap';
import '../App.css'

export default function Highlights(){
	return (
		<Row className="mt-3 mb-3 text-center">
			<Col xs={12} md={4} className="my-2">
				<Card className="cardImage">
				  <Card.Img className="img-fluid" variant="top" src="https://img.freepik.com/premium-photo/table-showcase-with-laptops-technology-store-choosing-buying-laptop-technology-store_245974-2795.jpg" />
				  <Card.Body>
				    <Card.Title>Laptops</Card.Title>
				    <Card.Text className="text-justify">
				      Discover powerful, sleek laptops with cutting-edge features for productivity and entertainment on our website. Get yours today!
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4} className="my-2">
				<Card className="cardImage">
				  <Card.Img className="img-fluid" variant="top" src="https://media.istockphoto.com/id/1316576464/photo/smartphones-store-showcase-with-selling-various-new-smartphones-in-electronics-store-during-a.jpg?s=612x612&w=0&k=20&c=t-tc_akc5bwQGOdh8fDv5h8wkYRWRn67gmLUQZ46fyY=" />
				  <Card.Body>
				    <Card.Title className="text-justify">Cellphones</Card.Title>
				    <Card.Text>
				      Explore a wide selection of Android and iPhone cellphones on our website, offering top-notch performance and stylish design.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4} className="my-2">
				<Card className="cardImage">
				  <Card.Img className="img-fluid" variant="top" src="https://builtin.com/sites/www.builtin.com/files/2023-06/tech%20support.jpg" />
				  <Card.Body>
				    <Card.Title>Services</Card.Title>
				    <Card.Text className="text-justify">
				      Get peace of mind with our exceptional after-sales support services for laptops, cellphones, and gadgets on our website.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

		</Row>	
	)
}
