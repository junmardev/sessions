console.log('Hello World Again - second time!');

// [ SECTIO ] Syntax, Statements and Comments
// JS Statements usually ends with semi-colon (;)

//Comments: 
// There are two types of Comments:
	//1. The single-line comment denoted by //
	//2. The multiple-line comment denoted by /* */

// [ SECTION ] Variables
	// It is used to contain data
	// Any information that is used by an application is stored in what we call a "memory"

	//Declaring Variables

	//Declaring Variables - tells our devices that a variable name is created.

	let myVariableNumber; // A naming convention
	console.log(myVariableNumber)

	//Syntax
		// let/const/ var variableName

	//Declaring and Initializing Variables

	let productName = "desktop computer";
	console.log(productName);
	let productPrice = 18900;
	console.log(productPrice);

	//const, impossible to reassign - constant
	const interest = 3.539;
	console.log(interest);

	//float vs double
	// both can store decimal numbers
	// float -> 1.1234567 up to 7
	// double -> 1.234567891011112 up to 15

	//Reassigning variable values

	productName = "laptop";
	console.log(productName);

	//Declare  the variable

	myVariableNumber = 2023;
	console.log(myVariableNumber);


	//Multiple Variable Declarations

	let productCode = 'DC017';
	const productBrand = 'Dell';
	console.log(productCode, productBrand);

	// [ SECTION ] Data Types
		//Strings

		let country = 'Philippines';
		let province = 'Cebu City';

		//Concatenation Strings

		let fullAddress = province + ', ' + country;
		console.log(fullAddress);

		let greeting = 'I live in the ' + country;
		console.log(greeting);

		let mailAddress = 'Cebut City \n\nPhilippines';
		console.log(mailAddress);

		let message = "John's employees went home early";
		console.log(message);

		message = 'John\'s employees went home early again!';
		console.log(message);

		//Numbers
		let headcount = 26; 
		console.log(headcount);

		//Decimal Numbers/Fractions
		let grade = 98.7;
		console.log(grade);

		//Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		//Combining text and strings
		console.log("John's grade last quarter is " + grade);

		//Boolean
		//Return true or false

		let isMarried = false;
		let inGoodConduct = true;
		console.log("isMarried: " + isMarried);
		console.log("inGoodConduct: " + inGoodConduct);

		//Arrays
		let grades = [98.7, 92.1, 90.2, 94.6]
		console.log(grades);
		console.log(grades[0]);
		let details = ["John", "Smith", 32, true];
		console.log(details);

		//Objects
		//Composed of "key/value pair"
		let person = {
			fullName: 'Junmar Letigio',
			age: 27,
			isMarried: false,
			contact: ['09913335664', '09926667741'],
			address: {
				houseNumber: '345',
				city: 'Cebu'
			}
		};
		console.log(person);
		console.log(person.fullName);
		console.log(person.age);
		console.log(person.isMarried);
		//We only use [] to call the index numbre of the data from the array
		console.log(person.contact[0]);
		console.log(person.contact[1]);
		console.log(person.contact);
		console.log(person.address.houseNumber);
		console.log(person.address.city);

		let arrays = [
			["hey", "hey1", "hey2"],
			["hey", "hey1", "hey2"],
			["hey", "hey1", "hey2"]
		]

		console.log(arrays);

		let myGrades = {
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.2,
			fourthGrading: 94.6
		};

		console.log(myGrades);

		//Typeof Operator

		console.log(typeof myGrades);
		console.log(typeof arrays);
		console.log(typeof greeting);

		//Null
		let spouse = null;
		let myNumber = 0;
		let myString = '';

		console.log(spouse);
		console.log(myNumber);
		console.log(myString);

		//Undefined
		let fullName;
		console.log(fullName);