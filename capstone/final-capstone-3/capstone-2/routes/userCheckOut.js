const express = require("express");
const checkOutController = require("../controllers/userCheckOut");
const auth = require("../auth.js")

// Destructure from auth
const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const router = express.Router();

//User checkout -> Non-admin/seller checkout (Creating/Placing an Order)
router.post("/user-checkout", verify, checkOutController.userCheckOut);

//User Order Summary
router.get("/order-summary", verify, checkOutController.orderSummary);

//Retrieve All Orders Seller/Admin Only
router.get("/allOrders", verify, verifyAdmin, checkOutController.getAllOrders);

//NEW ADDED
// Get 1 specific course using its ID
router.get("/:productId", checkOutController.getProduct);


module.exports = router;