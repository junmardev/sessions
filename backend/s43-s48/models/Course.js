// [SECTION] Dependencies and Modules
const mongoose = require("mongoose");

//[SECTION] Schema/Blueprint
const courseSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [ true, "Course is required!" ]
	},
	description : {
		type: String,
		required: [ true, "Description is required!" ]
	},
	price : {
		type: Number,
		required: [ true, "Pirce is required!" ]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		//The "new Date()" expression that instantiates a new "date" that store current time & date whenever a course is created in our database.
		default: new Date()
	},
	enrollees : [
		{
			userId : {
				type: String,
				required: [ true, "UserId is required!" ]
			},
			enrolledOn : {
				type: Date,
				default: new Date()
			}
		}
	]
});


//[SECTION] Model
module.exports = mongoose.model("Course", courseSchema);