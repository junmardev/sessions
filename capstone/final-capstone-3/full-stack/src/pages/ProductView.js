import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

  
export default function ProductView(){

	const {user} = useContext(UserContext);

	const {productId} = useParams();

	const navigate = useNavigate();
	
	const [image, setImage] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	 const [quantity, setQuantity] = useState(1); //

	const order = (productId) => {
		fetch('https://capstone2-letigio.onrender.com/checkOut/user-checkout', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				// Swal.fire({
				// 	title: "Successfully Order!",
				// 	icon: "success",
				// 	text: "You have successfully place an order!."
				// })

				// navigate("/products");
				navigate(`/checkout/${productId}/${quantity}`);
			}else {
				Swal.fire({
					title: "Something Went Wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	// Function to increment the quantity
	const incrementQuantity = () => {
	    setQuantity(quantity + 1);
	}

	 // Function to decrement the quantity (prevent going below 1)
	const decrementQuantity = () => {
	    if (quantity > 1) {
	      setQuantity(quantity - 1);
	    }
	 }

	useEffect(() => {
		console.log(productId)

		fetch(`https://capstone2-letigio.onrender.com/checkOut/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setImage(data.image);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])

	return (
		<Container className="mt-5">
                    <Row>
                        <Col lg={{ span: 6, offset: 3 }}>
                            <Card>
                                <Card.Body className="text-center">
                                	<div style={{ height: '20%', overflow: 'hidden' }}>
                                	  <img src={image} className="img-fluid" alt={name} style={{ width: '100%', objectFit: 'cover' }} />
                                	</div>
                                    <Card.Title>{name}</Card.Title>
                                    <Card.Subtitle>Description:</Card.Subtitle>
                                    <Card.Text>{description}</Card.Text>
                                    <Card.Subtitle>Price:</Card.Subtitle>
                                    <Card.Text>PhP {price}</Card.Text>

                                    <div className="mb-3">
                                    	<Card.Subtitle className="mb-1">Qantity: </Card.Subtitle>
						                <Button variant="info" onClick={incrementQuantity}>+</Button>
						                <span className="mx-2">{quantity}</span>
						                <Button variant="info" onClick={decrementQuantity}>-</Button>
					              	</div>

                                   {
                                    	user.id !== null ?
                                    	<>
                                    	 <Button variant="success" onClick={() => order(productId)} >Check-Out</Button>
                                    	 <Button variant="danger" className="mx-3" onClick={() => navigate('/products')}>Cancel</Button>
                                    	 </>
                                    	:
                                    	<Link className="btn btn-danger btn-block" to="/login">Login to Purchase</Link>
                                    }

                                    
                                </Card.Body>        
                            </Card>
                        </Col>
                    </Row>
        </Container>
	)
}